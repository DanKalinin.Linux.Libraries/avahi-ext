//
// Created by root on 29.09.2021.
//

#include "avhsservicebrowser.h"










AvahiSServiceBrowser *avh_s_service_browser_new(AvahiServer *server, AvahiIfIndex interface, AvahiProtocol protocol, gchar *type, gchar *domain, AvahiLookupFlags flags, AvahiSServiceBrowserCallback callback, gpointer data, GError **error) {
    AvahiSServiceBrowser *self = avahi_s_service_browser_new(server, interface, protocol, type, domain, flags, callback, data);

    if (self == NULL) {
        gint code = avahi_server_errno(server);
        avh_set_error(error, code);
    }

    return self;
}










enum {
    AVH_S_SERVICE_BROWSER_SIGNAL_EVENT = 1,
    _AVH_S_SERVICE_BROWSER_SIGNAL_COUNT
};

guint avh_s_service_browser_signals[_AVH_S_SERVICE_BROWSER_SIGNAL_COUNT] = {0};

void avh_s_service_browser_finalize(GObject *self);
void _avh_s_service_browser_event(AVHSServiceBrowser *self, AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, gchar *name, gchar *type, gchar *domain, AvahiLookupResultFlags flags);

G_DEFINE_TYPE(AVHSServiceBrowser, avh_s_service_browser, GE_TYPE_OBJECT)

static void avh_s_service_browser_class_init(AVHSServiceBrowserClass *class) {
    G_OBJECT_CLASS(class)->finalize = avh_s_service_browser_finalize;

    class->event = _avh_s_service_browser_event;

    avh_s_service_browser_signals[AVH_S_SERVICE_BROWSER_SIGNAL_EVENT] = g_signal_new("event", AVH_TYPE_S_SERVICE_BROWSER, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(AVHSServiceBrowserClass, event), NULL, NULL, NULL, G_TYPE_NONE, 8, G_TYPE_POINTER, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_INT);
}

static void avh_s_service_browser_init(AVHSServiceBrowser *self) {

}

void avh_s_service_browser_finalize(GObject *self) {
    avh_s_service_browser_set_object(AVH_S_SERVICE_BROWSER(self), NULL);

    G_OBJECT_CLASS(avh_s_service_browser_parent_class)->finalize(self);
}

void avh_s_service_browser_event_callback(AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, const gchar *name, const gchar *type, const gchar *domain, AvahiLookupResultFlags flags, gpointer self) {
    avh_s_service_browser_event(self, browser, interface, protocol, event, (gchar *)name, (gchar *)type, (gchar *)domain, flags);
}

void avh_s_service_browser_event(AVHSServiceBrowser *self, AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, gchar *name, gchar *type, gchar *domain, AvahiLookupResultFlags flags) {
    g_signal_emit(self, avh_s_service_browser_signals[AVH_S_SERVICE_BROWSER_SIGNAL_EVENT], 0, browser, interface, protocol, event, name, type, domain, flags);
}

void _avh_s_service_browser_event(AVHSServiceBrowser *self, AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, gchar *name, gchar *type, gchar *domain, AvahiLookupResultFlags flags) {
    g_print("event\n");
}
