//
// Created by root on 30.09.2021.
//

#include "avhsserviceresolver.h"










AvahiSServiceResolver *avh_s_service_resolver_new(AvahiServer *server, AvahiIfIndex interface, AvahiProtocol protocol, gchar *name, gchar *type, gchar *domain, AvahiProtocol desired, AvahiLookupFlags flags, AvahiSServiceResolverCallback callback, gpointer data, GError **error) {
    AvahiSServiceResolver *self = avahi_s_service_resolver_new(server, interface, protocol, name, type, domain, desired, flags, callback, data);

    if (self == NULL) {
        gint code = avahi_server_errno(server);
        avh_set_error(error, code);
    }

    return self;
}










enum {
    AVH_S_SERVICE_RESOLVER_SIGNAL_EVENT = 1,
    _AVH_S_SERVICE_RESOLVER_SIGNAL_COUNT
};

guint avh_s_service_resolver_signals[_AVH_S_SERVICE_RESOLVER_SIGNAL_COUNT] = {0};

void avh_s_service_resolver_finalize(GObject *self);
void _avh_s_service_resolver_event(AVHSServiceResolver *self, AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, gchar *name, gchar *type, gchar *domain, gchar *host, AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags);

G_DEFINE_TYPE(AVHSServiceResolver, avh_s_service_resolver, GE_TYPE_OBJECT)

static void avh_s_service_resolver_class_init(AVHSServiceResolverClass *class) {
    G_OBJECT_CLASS(class)->finalize = avh_s_service_resolver_finalize;

    class->event = _avh_s_service_resolver_event;

    avh_s_service_resolver_signals[AVH_S_SERVICE_RESOLVER_SIGNAL_EVENT] = g_signal_new("event", AVH_TYPE_S_SERVICE_RESOLVER, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(AVHSServiceResolverClass, event), NULL, NULL, NULL, G_TYPE_NONE, 12, G_TYPE_POINTER, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_UINT, G_TYPE_POINTER, G_TYPE_INT);
}

static void avh_s_service_resolver_init(AVHSServiceResolver *self) {

}

void avh_s_service_resolver_finalize(GObject *self) {
    avh_s_service_resolver_set_object(AVH_S_SERVICE_RESOLVER(self), NULL);

    G_OBJECT_CLASS(avh_s_service_resolver_parent_class)->finalize(self);
}

void avh_s_service_resolver_event_callback(AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, const gchar *name, const gchar *type, const gchar *domain, const gchar *host, const AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags, gpointer self) {
    avh_s_service_resolver_event(self, resolver, interface, protocol, event, (gchar *)name, (gchar *)type, (gchar *)domain, (gchar *)host, (AvahiAddress *)address, port, txt, flags);
}

void avh_s_service_resolver_event(AVHSServiceResolver *self, AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, gchar *name, gchar *type, gchar *domain, gchar *host, AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags) {
    g_signal_emit(self, avh_s_service_resolver_signals[AVH_S_SERVICE_RESOLVER_SIGNAL_EVENT], 0, resolver, interface, protocol, event, name, type, domain, host, address, port, txt, flags);
}

void _avh_s_service_resolver_event(AVHSServiceResolver *self, AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, gchar *name, gchar *type, gchar *domain, gchar *host, AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags) {
    g_print("event\n");
}
