//
// Created by root on 28.09.2021.
//

#include "avhserver.h"










AvahiServer *avh_server_new(AvahiPoll *poll, AvahiServerConfig *config, AvahiServerCallback callback, gpointer data, GError **error) {
    gint code = AVAHI_OK;
    AvahiServer *self = avahi_server_new(poll, config, callback, data, &code);

    if (self == NULL) {
        avh_set_error(error, code);
    }

    return self;
}

gint avh_server_add_service_strlst(AvahiServer *self, AvahiSEntryGroup *group, AvahiIfIndex interface, AvahiProtocol protocol, AvahiPublishFlags flags, gchar *name, gchar *type, gchar *domain, gchar *host, guint16 port, AvahiStringList *strlst, GError **error) {
    gint ret = avahi_server_add_service_strlst(self, group, interface, protocol, flags, name, type, domain, host, port, strlst);

    if (ret < AVAHI_OK) {
        avh_set_error(error, ret);
    }

    return ret;
}










enum {
    AVH_SERVER_SIGNAL_STATE = 1,
    _AVH_SERVER_SIGNAL_COUNT
};

guint avh_server_signals[_AVH_SERVER_SIGNAL_COUNT] = {0};

void avh_server_finalize(GObject *self);
void _avh_server_state(AVHServer *self, AvahiServer *server, AvahiServerState state);

G_DEFINE_TYPE(AVHServer, avh_server, GE_TYPE_OBJECT)

static void avh_server_class_init(AVHServerClass *class) {
    G_OBJECT_CLASS(class)->finalize = avh_server_finalize;

    class->state = _avh_server_state;

    avh_server_signals[AVH_SERVER_SIGNAL_STATE] = g_signal_new("state", AVH_TYPE_SERVER, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(AVHServerClass, state), NULL, NULL, NULL, G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_INT);
}

static void avh_server_init(AVHServer *self) {

}

void avh_server_finalize(GObject *self) {
    avh_server_set_object(AVH_SERVER(self), NULL);

    G_OBJECT_CLASS(avh_server_parent_class)->finalize(self);
}

void avh_server_state_callback(AvahiServer *server, AvahiServerState state, gpointer self) {
    avh_server_state(self, server, state);
}

void avh_server_state(AVHServer *self, AvahiServer *server, AvahiServerState state) {
    g_signal_emit(self, avh_server_signals[AVH_SERVER_SIGNAL_STATE], 0, server, state);
}

void _avh_server_state(AVHServer *self, AvahiServer *server, AvahiServerState state) {
    g_print("state\n");
}
