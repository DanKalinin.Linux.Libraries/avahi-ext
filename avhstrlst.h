//
// Created by root on 24.03.2021.
//

#ifndef LIBRARY_AVAHI_EXT_AVHSTRLST_H
#define LIBRARY_AVAHI_EXT_AVHSTRLST_H

#include "avhmain.h"

G_BEGIN_DECLS

G_DEFINE_AUTOPTR_CLEANUP_FUNC(AvahiStringList, avahi_string_list_free)

G_END_DECLS

#endif //LIBRARY_AVAHI_EXT_AVHSTRLST_H
