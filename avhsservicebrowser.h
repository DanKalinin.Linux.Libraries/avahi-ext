//
// Created by root on 29.09.2021.
//

#ifndef LIBRARY_AVAHI_EXT_AVHSSERVICEBROWSER_H
#define LIBRARY_AVAHI_EXT_AVHSSERVICEBROWSER_H

#include "avhmain.h"
#include "avherror.h"










G_BEGIN_DECLS

G_DEFINE_AUTOPTR_CLEANUP_FUNC(AvahiSServiceBrowser, avahi_s_service_browser_free)

AvahiSServiceBrowser *avh_s_service_browser_new(AvahiServer *server, AvahiIfIndex interface, AvahiProtocol protocol, gchar *type, gchar *domain, AvahiLookupFlags flags, AvahiSServiceBrowserCallback callback, gpointer data, GError **error);

G_END_DECLS










G_BEGIN_DECLS

#define AVH_TYPE_S_SERVICE_BROWSER avh_s_service_browser_get_type()

GE_DECLARE_DERIVABLE_TYPE(AVHSServiceBrowser, avh_s_service_browser, AVH, S_SERVICE_BROWSER, GEObject)

struct _AVHSServiceBrowserClass {
    GEObjectClass super;

    void (*event)(AVHSServiceBrowser *self, AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, gchar *name, gchar *type, gchar *domain, AvahiLookupResultFlags flags);
};

struct _AVHSServiceBrowser {
    GEObject super;

    AvahiSServiceBrowser *object;
};

GE_STRUCTURE_FIELD(avh_s_service_browser, object, AVHSServiceBrowser, AvahiSServiceBrowser, NULL, avahi_s_service_browser_free, NULL)

void avh_s_service_browser_event_callback(AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, const gchar *name, const gchar *type, const gchar *domain, AvahiLookupResultFlags flags, gpointer self);
void avh_s_service_browser_event(AVHSServiceBrowser *self, AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, gchar *name, gchar *type, gchar *domain, AvahiLookupResultFlags flags);

G_END_DECLS










#endif //LIBRARY_AVAHI_EXT_AVHSSERVICEBROWSER_H
