//
// Created by root on 29.09.2021.
//

#ifndef LIBRARY_AVAHI_EXT_AVHSENTRYGROUP_H
#define LIBRARY_AVAHI_EXT_AVHSENTRYGROUP_H

#include "avhmain.h"
#include "avherror.h"










G_BEGIN_DECLS

G_DEFINE_AUTOPTR_CLEANUP_FUNC(AvahiSEntryGroup, avahi_s_entry_group_free)

AvahiSEntryGroup *avh_s_entry_group_new(AvahiServer *server, AvahiSEntryGroupCallback callback, gpointer data, GError **error);
gint avh_s_entry_group_commit(AvahiSEntryGroup *self, GError **error);

G_END_DECLS










G_BEGIN_DECLS

#define AVH_TYPE_S_ENTRY_GROUP avh_s_entry_group_get_type()

GE_DECLARE_DERIVABLE_TYPE(AVHSEntryGroup, avh_s_entry_group, AVH, S_ENTRY_GROUP, GEObject)

struct _AVHSEntryGroupClass {
    GEObjectClass super;

    void (*state)(AVHSEntryGroup *self, AvahiServer *server, AvahiSEntryGroup *group, AvahiEntryGroupState state);
};

struct _AVHSEntryGroup {
    GEObject super;

    AvahiSEntryGroup *object;
};

GE_STRUCTURE_FIELD(avh_s_entry_group, object, AVHSEntryGroup, AvahiSEntryGroup, NULL, avahi_s_entry_group_free, NULL)

void avh_s_entry_group_state_callback(AvahiServer *server, AvahiSEntryGroup *group, AvahiEntryGroupState state, gpointer self);
void avh_s_entry_group_state(AVHSEntryGroup *self, AvahiServer *server, AvahiSEntryGroup *group, AvahiEntryGroupState state);

G_END_DECLS










#endif //LIBRARY_AVAHI_EXT_AVHSENTRYGROUP_H
