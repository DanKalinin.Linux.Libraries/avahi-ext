//
// Created by root on 28.09.2021.
//

#include "avhserverdefault.h"

void avh_server_default_finalize(GObject *self);

G_DEFINE_TYPE(AVHServerDefault, avh_server_default, AVH_TYPE_SERVER)

static void avh_server_default_class_init(AVHServerDefaultClass *class) {
    G_OBJECT_CLASS(class)->finalize = avh_server_default_finalize;
}

static void avh_server_default_init(AVHServerDefault *self) {

}

void avh_server_default_finalize(GObject *self) {
    avh_server_default_set_glib_poll(AVH_SERVER_DEFAULT(self), NULL);

    G_OBJECT_CLASS(avh_server_default_parent_class)->finalize(self);
}

AVHServerDefault *avh_server_default_new(GError **error) {
    g_autoptr(AVHServerDefault) self = g_object_new(AVH_TYPE_SERVER_DEFAULT, NULL);

    g_autoptr(AvahiGLibPoll) glib_poll = avahi_glib_poll_new(NULL, G_PRIORITY_LOW);
    AvahiPoll *poll = (AvahiPoll *)avahi_glib_poll_get(glib_poll);

    g_auto(AvahiServerConfig) config = {0};
    (void)avahi_server_config_init(&config);

    g_autoptr(AvahiServer) object = NULL;
    if ((object = avh_server_new(poll, &config, avh_server_state_callback, self, error)) == NULL) return NULL;

    AVH_SERVER(self)->object = g_steal_pointer(&object);
    self->glib_poll = g_steal_pointer(&glib_poll);

    return g_steal_pointer(&self);
}

AVHServerDefault *avh_server_default_shared(GError **error) {
    static AVHServerDefault *self = NULL;
    if (self != NULL) return self;
    self = avh_server_default_new(error);
    return self;
}
