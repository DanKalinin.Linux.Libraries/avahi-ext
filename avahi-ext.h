//
// Created by dan on 10.06.19.
//

#ifndef LIBRARY_AVAHI_EXT_AVAHI_EXT_H
#define LIBRARY_AVAHI_EXT_AVAHI_EXT_H

#include <avahi-ext/avhmain.h>
#include <avahi-ext/avherror.h>
#include <avahi-ext/avhstrlst.h>
#include <avahi-ext/avhglibpoll.h>
#include <avahi-ext/avhserver.h>
#include <avahi-ext/avhserverdefault.h>
#include <avahi-ext/avhsentrygroup.h>
#include <avahi-ext/avhsservicebrowser.h>
#include <avahi-ext/avhsserviceresolver.h>
#include <avahi-ext/avhinit.h>

#endif //LIBRARY_AVAHI_EXT_AVAHI_EXT_H
