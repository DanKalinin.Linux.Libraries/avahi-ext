//
// Created by root on 28.09.2021.
//

#ifndef LIBRARY_AVAHI_EXT_AVHSERVERDEFAULT_H
#define LIBRARY_AVAHI_EXT_AVHSERVERDEFAULT_H

#include "avhmain.h"
#include "avhserver.h"
#include "avhglibpoll.h"

G_BEGIN_DECLS

#define AVH_TYPE_SERVER_DEFAULT avh_server_default_get_type()

GE_DECLARE_DERIVABLE_TYPE(AVHServerDefault, avh_server_default, AVH, SERVER_DEFAULT, AVHServer)

struct _AVHServerDefaultClass {
    AVHServerClass super;
};

struct _AVHServerDefault {
    AVHServer super;

    AvahiGLibPoll *glib_poll;
};

GE_STRUCTURE_FIELD(avh_server_default, glib_poll, AVHServerDefault, AvahiGLibPoll, NULL, avahi_glib_poll_free, NULL)

AVHServerDefault *avh_server_default_new(GError **error);
AVHServerDefault *avh_server_default_shared(GError **error);

G_END_DECLS

#endif //LIBRARY_AVAHI_EXT_AVHSERVERDEFAULT_H
