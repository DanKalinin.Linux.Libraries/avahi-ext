//
// Created by root on 30.09.2021.
//

#ifndef LIBRARY_AVAHI_EXT_AVHSSERVICERESOLVER_H
#define LIBRARY_AVAHI_EXT_AVHSSERVICERESOLVER_H

#include "avhmain.h"
#include "avherror.h"










G_BEGIN_DECLS

G_DEFINE_AUTOPTR_CLEANUP_FUNC(AvahiSServiceResolver, avahi_s_service_resolver_free)

AvahiSServiceResolver *avh_s_service_resolver_new(AvahiServer *server, AvahiIfIndex interface, AvahiProtocol protocol, gchar *name, gchar *type, gchar *domain, AvahiProtocol desired, AvahiLookupFlags flags, AvahiSServiceResolverCallback callback, gpointer data, GError **error);

G_END_DECLS










G_BEGIN_DECLS

#define AVH_TYPE_S_SERVICE_RESOLVER avh_s_service_resolver_get_type()

GE_DECLARE_DERIVABLE_TYPE(AVHSServiceResolver, avh_s_service_resolver, AVH, S_SERVICE_RESOLVER, GEObject)

struct _AVHSServiceResolverClass {
    GEObjectClass super;

    void (*event)(AVHSServiceResolver *self, AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, gchar *name, gchar *type, gchar *domain, gchar *host, AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags);
};

struct _AVHSServiceResolver {
    GEObject super;

    AvahiSServiceResolver *object;
};

GE_STRUCTURE_FIELD(avh_s_service_resolver, object, AVHSServiceResolver, AvahiSServiceResolver, NULL, avahi_s_service_resolver_free, NULL)

void avh_s_service_resolver_event_callback(AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, const gchar *name, const gchar *type, const gchar *domain, const gchar *host, const AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags, gpointer self);
void avh_s_service_resolver_event(AVHSServiceResolver *self, AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, gchar *name, gchar *type, gchar *domain, gchar *host, AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags);

G_END_DECLS










#endif //LIBRARY_AVAHI_EXT_AVHSSERVICERESOLVER_H
