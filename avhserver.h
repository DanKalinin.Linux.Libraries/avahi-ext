//
// Created by root on 28.09.2021.
//

#ifndef LIBRARY_AVAHI_EXT_AVHSERVER_H
#define LIBRARY_AVAHI_EXT_AVHSERVER_H

#include "avhmain.h"
#include "avherror.h"










G_BEGIN_DECLS

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC(AvahiServerConfig, avahi_server_config_free)

G_END_DECLS










G_BEGIN_DECLS

G_DEFINE_AUTOPTR_CLEANUP_FUNC(AvahiServer, avahi_server_free)

AvahiServer *avh_server_new(AvahiPoll *poll, AvahiServerConfig *config, AvahiServerCallback callback, gpointer data, GError **error);
gint avh_server_add_service_strlst(AvahiServer *self, AvahiSEntryGroup *group, AvahiIfIndex interface, AvahiProtocol protocol, AvahiPublishFlags flags, gchar *name, gchar *type, gchar *domain, gchar *host, guint16 port, AvahiStringList *strlst, GError **error);

G_END_DECLS










G_BEGIN_DECLS

#define AVH_TYPE_SERVER avh_server_get_type()

GE_DECLARE_DERIVABLE_TYPE(AVHServer, avh_server, AVH, SERVER, GEObject)

struct _AVHServerClass {
    GEObjectClass super;

    void (*state)(AVHServer *self, AvahiServer *server, AvahiServerState state);
};

struct _AVHServer {
    GEObject super;

    AvahiServer *object;
};

GE_STRUCTURE_FIELD(avh_server, object, AVHServer, AvahiServer, NULL, avahi_server_free, NULL)

void avh_server_state_callback(AvahiServer *server, AvahiServerState state, gpointer self);
void avh_server_state(AVHServer *self, AvahiServer *server, AvahiServerState state);

G_END_DECLS










#endif //LIBRARY_AVAHI_EXT_AVHSERVER_H
