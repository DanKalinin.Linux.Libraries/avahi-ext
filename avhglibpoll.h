//
// Created by root on 07.11.2020.
//

#ifndef LIBRARY_AVAHI_EXT_AVHGLIBPOLL_H
#define LIBRARY_AVAHI_EXT_AVHGLIBPOLL_H

#include "avhmain.h"

G_BEGIN_DECLS

G_DEFINE_AUTOPTR_CLEANUP_FUNC(AvahiGLibPoll, avahi_glib_poll_free)

G_END_DECLS

#endif //LIBRARY_AVAHI_EXT_AVHGLIBPOLL_H
