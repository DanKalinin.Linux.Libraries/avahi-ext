//
// Created by dan on 10.06.19.
//

#ifndef LIBRARY_AVAHI_EXT_AVHMAIN_H
#define LIBRARY_AVAHI_EXT_AVHMAIN_H

#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-glib/glib-watch.h>
#include <avahi-core/core.h>
#include <avahi-core/lookup.h>
#include <avahi-core/publish.h>
#include <glib-ext/glib-ext.h>

#endif //LIBRARY_AVAHI_EXT_AVHMAIN_H
