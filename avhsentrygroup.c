//
// Created by root on 29.09.2021.
//

#include "avhsentrygroup.h"










AvahiSEntryGroup *avh_s_entry_group_new(AvahiServer *server, AvahiSEntryGroupCallback callback, gpointer data, GError **error) {
    AvahiSEntryGroup *self = avahi_s_entry_group_new(server, callback, data);

    if (self == NULL) {
        gint code = avahi_server_errno(server);
        avh_set_error(error, code);
    }

    return self;
}

gint avh_s_entry_group_commit(AvahiSEntryGroup *self, GError **error) {
    gint ret = avahi_s_entry_group_commit(self);

    if (ret < AVAHI_OK) {
        avh_set_error(error, ret);
    }

    return ret;
}










enum {
    AVH_S_ENTRY_GROUP_SIGNAL_STATE = 1,
    _AVH_S_ENTRY_GROUP_SIGNAL_COUNT
};

guint avh_s_entry_group_signals[_AVH_S_ENTRY_GROUP_SIGNAL_COUNT] = {0};

void avh_s_entry_group_finalize(GObject *self);
void _avh_s_entry_group_state(AVHSEntryGroup *self, AvahiServer *server, AvahiSEntryGroup *group, AvahiEntryGroupState state);

G_DEFINE_TYPE(AVHSEntryGroup, avh_s_entry_group, GE_TYPE_OBJECT)

static void avh_s_entry_group_class_init(AVHSEntryGroupClass *class) {
    G_OBJECT_CLASS(class)->finalize = avh_s_entry_group_finalize;

    class->state = _avh_s_entry_group_state;

    avh_s_entry_group_signals[AVH_S_ENTRY_GROUP_SIGNAL_STATE] = g_signal_new("state", AVH_TYPE_S_ENTRY_GROUP, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(AVHSEntryGroupClass, state), NULL, NULL, NULL, G_TYPE_NONE, 3, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_INT);
}

static void avh_s_entry_group_init(AVHSEntryGroup *self) {

}

void avh_s_entry_group_finalize(GObject *self) {
    avh_s_entry_group_set_object(AVH_S_ENTRY_GROUP(self), NULL);

    G_OBJECT_CLASS(avh_s_entry_group_parent_class)->finalize(self);
}

void avh_s_entry_group_state_callback(AvahiServer *server, AvahiSEntryGroup *group, AvahiEntryGroupState state, gpointer self) {
    avh_s_entry_group_state(self, server, group, state);
}

void avh_s_entry_group_state(AVHSEntryGroup *self, AvahiServer *server, AvahiSEntryGroup *group, AvahiEntryGroupState state) {
    g_signal_emit(self, avh_s_entry_group_signals[AVH_S_ENTRY_GROUP_SIGNAL_STATE], 0, server, group, state);
}

void _avh_s_entry_group_state(AVHSEntryGroup *self, AvahiServer *server, AvahiSEntryGroup *group, AvahiEntryGroupState state) {
    g_print("state\n");
}
