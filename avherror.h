//
// Created by dan on 11.06.19.
//

#ifndef LIBRARY_AVAHI_EXT_AVHERROR_H
#define LIBRARY_AVAHI_EXT_AVHERROR_H

#include "avhmain.h"

G_BEGIN_DECLS

#define AVH_ERROR avh_error_quark()

GQuark avh_error_quark(void);

void avh_set_error(GError **self, gint code);

G_END_DECLS

#endif //LIBRARY_AVAHI_EXT_AVHERROR_H
