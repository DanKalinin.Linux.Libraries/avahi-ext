//
// Created by root on 19.10.2021.
//

#ifndef LIBRARY_AVAHI_EXT_AVHINIT_H
#define LIBRARY_AVAHI_EXT_AVHINIT_H

#include "avhmain.h"

G_BEGIN_DECLS

void avh_init(void);

G_END_DECLS

#endif //LIBRARY_AVAHI_EXT_AVHINIT_H
