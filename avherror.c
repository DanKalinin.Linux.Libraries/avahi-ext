//
// Created by dan on 11.06.19.
//

#include "avherror.h"

G_DEFINE_QUARK(avh-error-quark, avh_error)

void avh_set_error(GError **self, gint code) {
    gchar *message = (gchar *)avahi_strerror(code);
    g_set_error_literal(self, AVH_ERROR, code, message);
}
